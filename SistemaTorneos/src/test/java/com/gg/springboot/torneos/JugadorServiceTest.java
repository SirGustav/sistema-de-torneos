package com.gg.springboot.torneos;

import static org.mockito.Mockito.when;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

import org.junit.Before;
import org.junit.Test;

import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.anyString;
import static org.mockito.Mockito.anyLong;
import static org.mockito.Mockito.anyInt;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.never;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;

import com.gg.torneos.model.Jugador;
import com.gg.torneos.repository.JugadorRepository;
import com.gg.torneos.service.JugadorService;

class JugadorServiceTest {
	@Autowired
	protected JugadorService jugadorService;
	
	@MockBean
	private JugadorRepository jugadorRepository;
	
	private List<Jugador> jugadores;
	
	@Before
	public void initJugadores() {
		MockitoAnnotations.initMocks(this);
		Jugador jug = new Jugador();
		jug.setNombre("Juan");
		jug.setApellido("Cordova");
		jug.setCiudad(12);
		jug.setCorreo("a@b.com");
		jug.setGamertag("Jhon117");
		jug.setIdj(3);
		jugadores.add(jug);
		
		jug = new Jugador();
		jug.setNombre("Anto");
		jug.setApellido("Nyan");
		jug.setCiudad(6);
		jug.setCorreo("b@c.com");
		jug.setGamertag("uwu");
		jug.setIdj(1);
		jugadores.add(jug);
	}
	
	@Test
	public void shouldDeleteById() {
		//Arrange
		
		
		
		
		//Assert
		verify(jugadorRepository, times(1)).deleteById(3);
		
	}
}

