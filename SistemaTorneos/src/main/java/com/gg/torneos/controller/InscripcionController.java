package com.gg.torneos.controller;

import java.util.Calendar;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.gg.torneos.model.Inscripcion;
import com.gg.torneos.model.Jugador;
import com.gg.torneos.service.InscripcionService;

@Controller
@RequestMapping("/inscripcion")
public class InscripcionController {
	@Autowired
	InscripcionService inscripcionService;
	
	@PostMapping("/anadirInscrito")
	public String make(Jugador jugador,@ModelAttribute Inscripcion inscripcion, Model model) {
		inscripcion.setEstadoLogico(true);
		inscripcion.setJugador(jugador.getIdj());
		inscripcion.setFechaInscripcion(Calendar.getInstance().getTime());
		inscripcion.setAdmin(true);
		System.out.println(inscripcion.toString());
		inscripcionService.save(inscripcion);
		return "exito";
	}
}
