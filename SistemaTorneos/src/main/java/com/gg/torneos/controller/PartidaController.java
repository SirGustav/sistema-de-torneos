package com.gg.torneos.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.gg.torneos.model.Partida;
import com.gg.torneos.service.PartidaService;

@Controller
@RequestMapping("/partida")
public class PartidaController {
	@Autowired
	PartidaService partidaService;
	
	@RequestMapping(method = RequestMethod.GET)
	public String elGet(Model model) {
		return "partida";
	}
	
	@GetMapping("/listar")
	public String listar(Model model) {
		model.addAttribute("partidas", partidaService.listAll());
		return "listarPartida";
	}
	
	@GetMapping("/crear")
	public String crear(Model model) {
		model.addAttribute("partida", new Partida());
		return "formPartida";
	}
	
	@PostMapping("/anadirPartida")
	public String make(@ModelAttribute Partida partida, Model model) {
		partidaService.save(partida);
		return "showMessagePartida";
	}
	
	@RequestMapping(value="/update/{idp}", method=RequestMethod.GET)
	public String editar(@PathVariable("idp") Integer idp, Model model){
	    model.addAttribute("partida", partidaService.findById(idp));
	    return "formUpdatePartida";
	}
	
	@GetMapping("/delete/{idp}")
	public String delete(@PathVariable("idp")Integer idp) {
		partidaService.delete(idp);
		return "redirect:/partida/listar";
	}
}
