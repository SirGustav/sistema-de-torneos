package com.gg.torneos.controller;

import java.util.Calendar;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.gg.torneos.model.Inscripcion;
import com.gg.torneos.model.Jugador;
import com.gg.torneos.service.CiudadService;
import com.gg.torneos.service.InscripcionService;
import com.gg.torneos.service.JugadorService;
import com.gg.torneos.service.TorneoService;

@Controller
@RequestMapping("/jugador")
public class JugadorController {
	@Autowired
	private JugadorService jugadorService;
	
	@Autowired
	private CiudadService ciudadService;
	
	@Autowired
	private TorneoService torneoService;
	
	@Autowired
	private InscripcionService inscripcionService;
	
	@RequestMapping(method = RequestMethod.GET)
	public String index(Model model) {
		return "jugador";
	}
	
	@GetMapping("/creado")
	public String creado(Model model) {
		model.addAttribute("jugadores", jugadorService.listAll());
		model.addAttribute("torneos", torneoService.findByOrderByFechaInicioAsc());
		model.addAttribute("inscripcion", new Inscripcion());
		return "formExistente";
	}
	
	@GetMapping("/listar")
	public String listar(Model model) {
		//Para listar, no necesitamos las ciudades ordenadas por nombre, sino por su id
		model.addAttribute("ciudades", ciudadService.listAll());
		model.addAttribute("jugadores", jugadorService.listAll());
		model.addAttribute("inscripciones", inscripcionService.listAll());
		return "listarJugador";
	}
	
	@GetMapping("/crear")
	public String crear(Model model) {
		model.addAttribute("ciudades", ciudadService.findByOrderByNombreCAsc());
		model.addAttribute("torneos", torneoService.findByOrderByFechaInicioAsc());
		model.addAttribute("jugador", new Jugador());
		model.addAttribute("inscripcion", new Inscripcion());
		return "formJugador";
	}
	
	@PostMapping("/anadirJugador")
	public String make(@ModelAttribute Jugador jugador, Inscripcion inscripcion, Model model) {
		jugador.setEstadoLogico(true);
		jugador.setVictorias(0);
		jugador.setDerrotas(0);
		jugadorService.save(jugador);
		inscripcion.setEstadoLogico(true);
		inscripcion.setJugador(jugador.getIdj());
		inscripcion.setFechaInscripcion(Calendar.getInstance().getTime());
		inscripcion.setAdmin(true);
		System.out.println(inscripcion.toString());
		inscripcionService.save(inscripcion);
		model.addAttribute("ciudades", ciudadService.listAll());
		return "exito";
	}
	
	@RequestMapping(value="/update/{id}", method=RequestMethod.GET)
	public String editar(@PathVariable("id") Integer id, Model model){
		model.addAttribute("ciudades", ciudadService.findByOrderByNombreCAsc());
	    model.addAttribute("jugador", jugadorService.findById(id));
	    return "formUpdateJugador";
	}
	
	@GetMapping("/delete/{id}")
	public String delete(@PathVariable("id")Integer id) {
		jugadorService.delete(id);
		return "redirect:/jugador/listar";
	}
}