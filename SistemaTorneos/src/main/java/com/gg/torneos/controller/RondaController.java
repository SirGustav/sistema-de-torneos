package com.gg.torneos.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.gg.torneos.model.Ronda;
import com.gg.torneos.service.RondaService;

@Controller
@RequestMapping("/ronda")
public class RondaController {
	@Autowired
	RondaService rondaService;
	
	@RequestMapping(method = RequestMethod.GET)
	public String elGet(Model model) {
		return "ronda";
	}
	
	@GetMapping("/listar")
	public String listar(Model model) {
		model.addAttribute("rondas", rondaService.listAll());
		return "listarRonda";
	}
	
	@GetMapping("/crear")
	public String crear(Model model) {
		model.addAttribute("ronda", new Ronda());
		return "formRonda";
	}
	
	@PostMapping("/anadirRonda")
	public String make(@ModelAttribute Ronda ronda, Model model) {
		ronda.setEstado(false);
		rondaService.save(ronda);
		return "showMessageRonda";
	}
	
	@RequestMapping(value="/update/{idr}", method=RequestMethod.GET)
	public String editar(@PathVariable("idr") Integer idr, Model model){
	    model.addAttribute("ronda", rondaService.findById(idr));
	    return "formUpdateRonda";
	}
	
	@GetMapping("/delete/{idt}")
	public String delete(@PathVariable("idt")Integer idt) {
		rondaService.delete(idt);
		return "redirect:/ronda/listar";
	}
}
