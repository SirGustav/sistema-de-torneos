package com.gg.torneos.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.gg.torneos.service.InscripcionService;
import com.gg.torneos.service.JugadorService;
import com.gg.torneos.service.TorneoService;

@Controller
@RequestMapping("/bracket")
public class BracketController {
	@Autowired
	TorneoService torneoService;
	@Autowired
	InscripcionService inscripcionService;
	@Autowired
	JugadorService jugadorService;
	
	@GetMapping("/go")
	public String listar(Model model) {
		return "go";
	}
	
	@RequestMapping(value="/go/{idt}", method=RequestMethod.GET)
	public String bracket(@PathVariable("idt") Integer idt, Model model){
		List<Integer> listaJugadores = inscripcionService.findJugadoresByIdt(idt);
		List<String> listaGT = new ArrayList<String>();
		for(int i=0; i<listaJugadores.size(); i++) {
			 listaGT.add(jugadorService.findById(listaJugadores.get(i)).getGamertag());
		}
		model.addAttribute("gamertags", listaGT);
		model.addAttribute("totalInscritos", listaGT.size());
	    return "go";
	}
}
