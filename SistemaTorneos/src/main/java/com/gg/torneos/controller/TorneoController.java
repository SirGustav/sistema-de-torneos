package com.gg.torneos.controller;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;

import com.gg.torneos.model.Torneo;
import com.gg.torneos.service.CiudadService;
import com.gg.torneos.service.TorneoService;

import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("/torneo")
public class TorneoController {

	@Autowired
	TorneoService torneoService;
	
	@Autowired
	CiudadService ciudadService;
	
	@RequestMapping(method = RequestMethod.GET)
	public String elGet(Model model) {
		return "torneo";
	}
	
	@GetMapping("/listar")
	public String listar(Model model) {
		//Para listar, no necesitamos las ciudades ordenadas por nombre, sino por su id
		model.addAttribute("ciudades", ciudadService.listAll());
		model.addAttribute("torneos", torneoService.findByOrderByFechaInicioAsc());
		return "listarTorneo";
	}
	
	@GetMapping("/crear")
	public String crear(Model model) {
		Date hoy = new Date();
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		model.addAttribute("ciudades", ciudadService.findByOrderByNombreCAsc());
		model.addAttribute("torneo", new Torneo());
		System.out.println(formatter.format(hoy));
		model.addAttribute("hoy", formatter.format(hoy));
		return "formTorneo";
	}
	
	@PostMapping("/anadirTorneo")
	public String make(@ModelAttribute Torneo torneo, Model model) {
		torneo.setGanadorFinal(null);
		torneoService.save(torneo);
		model.addAttribute("ciudades", ciudadService.listAll());
		return "showMessageTorneo";
	}
	
	@RequestMapping(value="/update/{idt}", method=RequestMethod.GET)
	public String editar(@PathVariable("idt") Integer idt, Model model){
		model.addAttribute("ciudades", ciudadService.findByOrderByNombreCAsc());
	    model.addAttribute("torneo", torneoService.findById(idt));
	    return "formUpdateTorneo";
	}
	
	@GetMapping("/delete/{idt}")
	public String delete(@PathVariable("idt")Integer idt) {
		torneoService.delete(idt);
		return "redirect:/torneo/listar";
	}
}
