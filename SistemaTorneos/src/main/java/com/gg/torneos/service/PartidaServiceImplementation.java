package com.gg.torneos.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gg.torneos.model.Partida;
import com.gg.torneos.repository.PartidaRepository;

@Service
public class PartidaServiceImplementation implements PartidaService{
	@Autowired
	private PartidaRepository partidaRepository;

	@Override
	public List<Partida> listAll() {
		return partidaRepository.findAll();
	}

	@Override
	public void save(Partida partida) {
		partidaRepository.save(partida);		
	}

	@Override
	public Partida findById(Integer id) {
		return partidaRepository.getOne(id);
	}

	@Override
	public void delete(Integer id) {
		partidaRepository.deleteById(id);		
	}

}
