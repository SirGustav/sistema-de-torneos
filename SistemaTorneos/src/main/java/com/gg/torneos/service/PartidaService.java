package com.gg.torneos.service;

import java.util.List;

import com.gg.torneos.model.Partida;

public interface PartidaService {
	List<Partida> listAll();
	void save(Partida partida);
	Partida findById(Integer id);
	void delete(Integer id);
}
