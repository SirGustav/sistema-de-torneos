package com.gg.torneos.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.gg.torneos.model.Inscripcion;
import com.gg.torneos.repository.InscripcionRepository;

@Service
public class InscripcionServiceImplementation implements InscripcionService{
	@Autowired
	private InscripcionRepository inscripcionRepository;
	
	@Override
	public List<Inscripcion> listAll() {
		return inscripcionRepository.findAll();
	}

	@Override
	public List<Inscripcion> findByOrderByTorneoAsc() {
		return inscripcionRepository.findAll(Sort.by(Sort.Direction.ASC, "torneo"));
	}

	@Override
	public void save(Inscripcion inscripcion) {
		inscripcionRepository.save(inscripcion);		
	}

	@Override
	public Inscripcion findById(Integer id) {
		return inscripcionRepository.getOne(id);
	}

	@Override
	public void delete(Integer id) {
		inscripcionRepository.deleteById(id);
	}

	@Override
	public List<Integer> findJugadoresByIdt(Integer idt) {
		List<Integer> jugadores = new ArrayList<Integer>();
		List<Inscripcion> todo = inscripcionRepository.findAll();
		for(int i=0; i<todo.size(); i++) {
			if(todo.get(i).getTorneo() == idt) jugadores.add(todo.get(i).getJugador());
		}
		return jugadores;
	}

}
