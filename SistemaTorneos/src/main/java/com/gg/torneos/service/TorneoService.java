package com.gg.torneos.service;

import java.util.List;

import com.gg.torneos.model.Torneo;

public interface TorneoService {
	List<Torneo> listAll();
	List<Torneo> findByOrderByFechaInicioAsc();
	void save(Torneo torneo);
	Torneo findById(Integer id);
	void delete(Integer id);
}
