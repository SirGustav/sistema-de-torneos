package com.gg.torneos.service;

import java.util.List;

import com.gg.torneos.model.Ciudad;

public interface CiudadService {
	List<Ciudad> listAll();
	List<Ciudad> findByOrderByNombreCAsc();
	void save(Ciudad ciudad);
	Ciudad findById(Integer id);
	void delete(Integer id);
}
