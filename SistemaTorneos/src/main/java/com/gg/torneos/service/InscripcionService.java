package com.gg.torneos.service;

import java.util.List;

import com.gg.torneos.model.Inscripcion;

public interface InscripcionService {
	List<Inscripcion> listAll();
	List<Inscripcion> findByOrderByTorneoAsc();
	void save(Inscripcion inscripcion);
	Inscripcion findById(Integer id);
	List<Integer> findJugadoresByIdt(Integer idt);
	void delete(Integer id);
}
