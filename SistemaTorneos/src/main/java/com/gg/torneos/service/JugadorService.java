package com.gg.torneos.service;

import java.util.List;

import com.gg.torneos.model.Jugador;

public interface JugadorService {
	List<Jugador> listAll();
	void save(Jugador customer);
	Jugador findById(Integer id);
	void delete(Integer id);
}
