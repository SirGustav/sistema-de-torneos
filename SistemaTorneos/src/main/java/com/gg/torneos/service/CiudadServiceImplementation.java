package com.gg.torneos.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.gg.torneos.model.Ciudad;
import com.gg.torneos.repository.CiudadRepository;

@Service
public class CiudadServiceImplementation implements CiudadService{
	@Autowired
	private CiudadRepository ciudadRepository;
	
	@Override
	public List<Ciudad> listAll() {
		return ciudadRepository.findAll();
	}

	@Override
	public void save(Ciudad ciudad) {
		ciudadRepository.save(ciudad);
	}

	@Override
	public Ciudad findById(Integer id) {
		return ciudadRepository.getOne(id);
	}

	@Override
	public void delete(Integer id) {
		ciudadRepository.deleteById(id);	
	}

	@Override
	public List<Ciudad> findByOrderByNombreCAsc() {
		return ciudadRepository.findAll(Sort.by(Sort.Direction.ASC, "nombreC"));
	}

}
