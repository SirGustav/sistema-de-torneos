package com.gg.torneos.service;

import java.util.List;

import com.gg.torneos.model.Ronda;

public interface RondaService {
	List<Ronda> listAll();
	void save(Ronda ronda);
	Ronda findById(Integer id);
	void delete(Integer id);
}
