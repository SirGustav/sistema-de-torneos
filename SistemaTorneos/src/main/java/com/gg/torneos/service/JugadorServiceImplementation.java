package com.gg.torneos.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gg.torneos.model.Jugador;
import com.gg.torneos.repository.JugadorRepository;

@Service
public class JugadorServiceImplementation implements JugadorService{
	@Autowired
	private JugadorRepository jugadorRepository;

	@Override
	public List<Jugador> listAll() {
		return jugadorRepository.findAll();
	}
	
	@Override
	public void save(Jugador jugador) {
		jugadorRepository.save(jugador);
	}

	@Override
	public Jugador findById(Integer id) {
		return jugadorRepository.getOne(id);
	}

	@Override
	public void delete(Integer id) {
		jugadorRepository.deleteById(id);
		
	}
}
