package com.gg.torneos.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gg.torneos.model.Ronda;
import com.gg.torneos.repository.RondaRepository;

@Service
public class RondaServiceImplementation implements RondaService{
	@Autowired
	private RondaRepository rondaRepository;

	@Override
	public List<Ronda> listAll() {
		return rondaRepository.findAll();
	}

	@Override
	public void save(Ronda ronda) {
		rondaRepository.save(ronda);		
	}

	@Override
	public Ronda findById(Integer id) {
		return rondaRepository.getOne(id);
	}

	@Override
	public void delete(Integer id) {
		rondaRepository.deleteById(id);		
	}

}
