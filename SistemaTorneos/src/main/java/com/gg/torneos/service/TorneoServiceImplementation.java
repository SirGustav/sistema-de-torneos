package com.gg.torneos.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.gg.torneos.model.Torneo;
import com.gg.torneos.repository.TorneoRepository;

@Service
public class TorneoServiceImplementation implements TorneoService{
	@Autowired
	private TorneoRepository torneoRepository;
	
	@Override
	public List<Torneo> listAll() {
		return torneoRepository.findAll();
	}

	@Override
	public void save(Torneo torneo) {
		torneoRepository.save(torneo);
	}

	@Override
	public Torneo findById(Integer id) {
		return torneoRepository.getOne(id);
	}

	@Override
	public void delete(Integer id) {
		torneoRepository.deleteById(id);
	}

	@Override
	public List<Torneo> findByOrderByFechaInicioAsc() {
		return torneoRepository.findAll(Sort.by(Sort.Direction.ASC, "fechaInicio"));
	}

}
