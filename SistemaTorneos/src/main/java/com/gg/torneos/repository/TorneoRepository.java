package com.gg.torneos.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.gg.torneos.model.Torneo;

@Repository
public interface TorneoRepository extends JpaRepository<Torneo,Integer> {

}
