package com.gg.torneos.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.gg.torneos.model.Jugador;

@Repository
	public interface JugadorRepository extends JpaRepository<Jugador, Integer>{
		
	}

