package com.gg.torneos.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import com.gg.torneos.model.Partida;

public interface PartidaRepository extends JpaRepository<Partida, Integer>{

}
