package com.gg.torneos.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.gg.torneos.model.Ciudad;

public interface CiudadRepository extends JpaRepository<Ciudad,Integer> {

}
