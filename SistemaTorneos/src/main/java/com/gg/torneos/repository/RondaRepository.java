package com.gg.torneos.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import com.gg.torneos.model.Ronda;

public interface RondaRepository extends JpaRepository<Ronda, Integer>{

}
