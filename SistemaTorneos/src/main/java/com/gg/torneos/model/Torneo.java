package com.gg.torneos.model;

import java.util.Date;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Table(name= "torneo")

public class Torneo{

	@Id
	@GeneratedValue
	private Integer idt;
	
	@NotEmpty
	private String nombreT;
	
	@NotEmpty
	private String descripcion;
	
	@NotNull
	@Column(name="idc")
	private int ciudad;
	
	@Column(name="ganador_final")
	private String ganadorFinal;
	
	@NotNull
	@Temporal(TemporalType.DATE)
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	@Column(name="fecha_inicio")
	private Date fechaInicio;
	
	@NotNull
	@Temporal(TemporalType.DATE)
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	@Column(name="fecha_termino")
	private Date fechaTermino;
		
	public Torneo() {
		// TODO Auto-generated constructor stub
	}

	public Torneo(String nombreT, String descripcion, int ciudad,
			String ganadorFinal, Date fechaInicio, Date fechaTermino) {
		this.nombreT = nombreT;
		this.descripcion = descripcion;
		this.ciudad = ciudad;
		this.ganadorFinal = ganadorFinal;
		this.fechaInicio = fechaInicio;
		this.fechaTermino = fechaTermino;
	}

	public Integer getIdt() {
		return idt;
	}

	public void setIdt(Integer idt) {
		this.idt = idt;
	}

	public String getNombreT() {
		return nombreT;
	}

	public void setNombreT(String nombreT) {
		this.nombreT = nombreT;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public int  getCiudad() {
		return ciudad;
	}

	public void setCiudad(int ciudad) {
		this.ciudad = ciudad;
	}

	public String getGanadorFinal() {
		return ganadorFinal;
	}

	public void setGanadorFinal(String ganadorFinal) {
		this.ganadorFinal = ganadorFinal;
	}

	public Date getFechaInicio() {
		return fechaInicio;
	}

	public void setFechaInicio(Date fechaInicio) {
		this.fechaInicio = fechaInicio;
	}

	public Date getFechaTermino() {
		return fechaTermino;
	}

	public void setFechaTermino(Date fechaTermino) {
		this.fechaTermino = fechaTermino;
	}
}
