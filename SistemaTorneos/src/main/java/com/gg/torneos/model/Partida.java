package com.gg.torneos.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Table(name = "partida")
public class Partida {

	@Id
	@GeneratedValue
	private Integer idp;
	
	@NotEmpty
	private String GTGanador;
	
	@NotEmpty
	private String GTPerdedor;
	
	@NotNull
	@Temporal(TemporalType.DATE)
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date fecha;
	
	/* Clave foránea por ver
	 * @NotNull
	private Integer idr; */ 
	
	public Partida() {}

	public Partida(Integer idp, @NotEmpty String gTGanador, @NotEmpty String gTPerdedor, @NotNull Date fecha) {
		this.idp = idp;
		GTGanador = gTGanador;
		GTPerdedor = gTPerdedor;
		this.fecha = fecha;
	}

	public Integer getIdp() {
		return idp;
	}

	public void setIdp(Integer idp) {
		this.idp = idp;
	}

	public String getGTGanador() {
		return GTGanador;
	}

	public void setGTGanador(String gTGanador) {
		GTGanador = gTGanador;
	}

	public String getGTPerdedor() {
		return GTPerdedor;
	}

	public void setGTPerdedor(String gTPerdedor) {
		GTPerdedor = gTPerdedor;
	}

	public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	/*public Integer getIdr() {
		return idr;
	}

	public void setIdr(Integer idr) {
		this.idr = idr;
	}*/
	
	
	
	
}
