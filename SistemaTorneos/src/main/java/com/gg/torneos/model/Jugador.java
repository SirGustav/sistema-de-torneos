package com.gg.torneos.model;

import java.io.Serializable;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(name = "jugador")

public class Jugador implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 5028245395106344209L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
    private Integer idj;

	@NotEmpty
	@Size(max = 50, message = "Nombre invalido")
	private String nombre;
	@NotEmpty
	@Size(max = 50, message = "Apellido invalido")
	private String 	apellido;
	@NotEmpty
	@Email
	private String correo;
	@NotNull
	@Column(name="idc")
	private int ciudad;
	@NotEmpty
	@Size(max = 150, message = "Descripcion demasiado larga")
	private String descripcion;
	@NotEmpty
	@Size(max = 40, message = "GamerTag demasiado largo")
	private String gamertag;
	/*
	@NotEmpty
	@Size(max = 150, message = "Muchos personajes")
	private String personajes;
	*/
	private int victorias;
	private int derrotas;
	@Column(name="estado_logico")
	private boolean estadoLogico;
		
	public Jugador() {

	}

	public Jugador(Integer idj, @NotEmpty @Size(min = 4, max = 50, message = "Nombre invalido") String nombre,
			@NotEmpty @Size(min = 4, max = 50, message = "Apellido invalido") String apellido,
			@NotEmpty @Email String correo,
			int ciudad,
			@NotEmpty @Size(max = 150, message = "Descripcion demasiado larga") String descripcion,
			@NotEmpty @Size(max = 40, message = "GamerTag demasiado largo") String gamertag) {
		this.idj = idj;
		this.nombre = nombre;
		this.apellido = apellido;
		this.correo = correo;
		this.ciudad = ciudad;
		this.descripcion = descripcion;
		this.gamertag = gamertag;
		this.victorias = 0;
		this.derrotas = 0;
		this.estadoLogico = true;
	}


	public Integer getIdj() {
		return idj;
	}



	public void setIdj(Integer idj) {
		this.idj = idj;
	}



	public String getNombre() {
		return nombre;
	}



	public void setNombre(String nombre) {
		this.nombre = nombre;
	}



	public String getApellido() {
		return apellido;
	}



	public void setApellido(String apellido) {
		this.apellido = apellido;
	}



	public String getCorreo() {
		return correo;
	}



	public void setCorreo(String correo) {
		this.correo = correo;
	}


	public int getCiudad() {
		return ciudad;
	}

	public void setCiudad(int ciudad) {
		this.ciudad = ciudad;
	}

	public String getDescripcion() {
		return descripcion;
	}



	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}



	public String getGamertag() {
		return gamertag;
	}



	public void setGamertag(String gamertag) {
		this.gamertag = gamertag;
	}


	public int getVictorias() {
		return victorias;
	}



	public void setVictorias(int victorias) {
		this.victorias = victorias;
	}



	public int getDerrotas() {
		return derrotas;
	}



	public void setDerrotas(int derrotas) {
		this.derrotas = derrotas;
	}



	public boolean isEstadoLogico() {
		return estadoLogico;
	}



	public void setEstadoLogico(boolean estadoLogico) {
		this.estadoLogico = estadoLogico;
	}
	
	
}
