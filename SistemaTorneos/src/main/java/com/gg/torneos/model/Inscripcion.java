package com.gg.torneos.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Table(name= "inscripcion")
public class Inscripcion implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -1991970659533497337L;
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer idi;
	@NotNull
	@Temporal(TemporalType.DATE)
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	@Column(name="fecha_inscripcion")
	private Date fechaInscripcion;
	@Column(name="estado_logico")
	private boolean estadoLogico;
	@NotNull
	@Column(name="idj")
	private int jugador;
	@NotNull
	@Column(name="idt")
	private int torneo;
	@Column(name="admin")
	private boolean admin;
	public Inscripcion() {
		
	}
	public Inscripcion(@NotNull Date fechaInscripcion, boolean estadoLogico, @NotNull int jugador, @NotNull int torneo,
			boolean admin) {
		this.fechaInscripcion = fechaInscripcion;
		this.estadoLogico = estadoLogico;
		this.jugador = jugador;
		this.torneo = torneo;
		this.admin = admin;
	}
	public Integer getIdi() {
		return idi;
	}
	public void setIdi(Integer idi) {
		this.idi = idi;
	}
	public Date getFechaInscripcion() {
		return fechaInscripcion;
	}
	public void setFechaInscripcion(Date fechaInscripcion) {
		this.fechaInscripcion = fechaInscripcion;
	}
	public boolean isEstadoLogico() {
		return estadoLogico;
	}
	public void setEstadoLogico(boolean estadoLogico) {
		this.estadoLogico = estadoLogico;
	}
	public int getJugador() {
		return jugador;
	}
	public void setJugador(int jugador) {
		this.jugador = jugador;
	}
	public int getTorneo() {
		return torneo;
	}
	public void setTorneo(int torneo) {
		this.torneo = torneo;
	}
	public boolean isAdmin() {
		return admin;
	}
	public void setAdmin(boolean admin) {
		this.admin = admin;
	}	
}
