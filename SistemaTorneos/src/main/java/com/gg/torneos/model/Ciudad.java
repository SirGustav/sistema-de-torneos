package com.gg.torneos.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;

@Entity
@Table(name= "ciudad")
public class Ciudad {
	@Id
	@GeneratedValue
	private Integer idc;
	@NotEmpty
	@Column(name="nombre")
	private String nombreC;
	
	public Ciudad() {
		// TODO Auto-generated constructor stub
	}

	public Ciudad(Integer idc, @NotEmpty String nombreC) {
		this.idc = idc;
		this.nombreC = nombreC;
	}

	public Integer getIdc() {
		return idc;
	}

	public void setIdc(Integer idc) {
		this.idc = idc;
	}

	public String getNombreC() {
		return nombreC;
	}

	public void setNombreC(String nombreC) {
		this.nombreC = nombreC;
	}
}
