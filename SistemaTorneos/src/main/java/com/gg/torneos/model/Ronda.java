package com.gg.torneos.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;

@Entity
@Table(name = "ronda")
public class Ronda {
	@Id
	@GeneratedValue
	private Integer idr;
	
	@NotEmpty
	private String tipoRonda;
	
	private boolean estado;
	
	public Ronda() {};

	public Ronda(Integer idr, @NotEmpty String tipoRonda, boolean estado) {
		super();
		this.idr = idr;
		this.tipoRonda = tipoRonda;
		this.estado = estado;
	}

	public Integer getIdr() {
		return idr;
	}

	public void setIdr(Integer idr) {
		this.idr = idr;
	}

	public String getTipoRonda() {
		return tipoRonda;
	}

	public void setTipoRonda(String tipoRonda) {
		this.tipoRonda = tipoRonda;
	}

	public boolean isEstado() {
		return estado;
	}

	public void setEstado(boolean estado) {
		this.estado = estado;
	}
	
	
}
